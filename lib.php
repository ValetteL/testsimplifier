<?php
/**
 * Block paramtest lib
 *
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 */

require_once ('class/preset.php');

/**
 * Read preset json file
 *
 * @return array
 */
function get_presets() : array
{
    $file = __DIR__ . '/json/presets.json';
    $data = file_get_contents($file);

    $json = json_decode($data, true);

    $presets = array();

    if($json) {
        foreach($json as $d) {
            $presets[] = new Preset($d);
        }
        // Reindex the values of the array from 1
        // (because 0 is considered like null so it will not go through some conditions)
        $presets = array_combine(range(1, count($presets)), array_values($presets));
    }

    return $presets;
}

/**
 * Overwrite preset json file
 *
 * @param $presets
 */
function set_presets($presets) {
    $file = __DIR__ . '/json/presets.json';

    file_put_contents($file, json_encode($presets));
}