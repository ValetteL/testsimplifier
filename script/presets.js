window.onload = init;

function init()
{
    let prefixesreview = ["during", "immediately", "open", "closed"];
    let prefixes = ["attempt", "correctness", "marks", "specificfeedback", "generalfeedback", "rightanswer", "overallfeedback"];

    $.getJSON("./json/presets.json", function(data) {
        $("#id_presets").change(function() {
            $.each(data[$(this).val()], function(key, value) {
                //console.log("key : " + key + ", value : " + value);
                /*prefixesreview.forEach(prefixreview => {
                    prefixes.forEach(prefix => {
                        if (key === "review" + prefixreview + "chk" && value === 0) {
                            console.log("key : " + key + ", value : " + value);
                            $("input[name=" + prefix + prefixreview + "]").attr("disabled", true);
                        } else if (key === "review" + prefixreview + "chk" && value === 1) {
                            $("input[name=" + prefix + prefixreview + "]").attr("disabled", false);
                        }
                    });
                });*/
                if (key === "timeopenchk" && value === 0) {
                    $("#id_timeopen_day").prop('disabled');
                    $("#id_timeopen_month").prop('disabled');
                    $("#id_timeopen_year").prop('disabled');
                    $("#id_timeopen_hour").prop('disabled');
                    $("#id_timeopen_minute").prop('disabled');
                } else if (key === "timeclosechk" && value === 0) {
                    $("#id_timeclose_day").prop('disabled');
                    $("#id_timeclose_month").prop('disabled');
                    $("#id_timeclose_year").prop('disabled');
                    $("#id_timeclose_hour").prop('disabled');
                    $("#id_timeclose_minute").prop('disabled');
                } else if (key !== 'name' && key !== 'submitbutton') {
                    let selector = $("input[name=" + key + "]");
                    if(selector.length) {
                        selector.val(value);
                    }
                }
            });
        });
    });


}