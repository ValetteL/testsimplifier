<?php
/**
 * Version file.
 *
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 */

$plugin->component = 'block_paramtest';
$plugin->version = 2011062835;
$plugin->requires = 2010112400;
