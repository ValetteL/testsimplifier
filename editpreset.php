<?php
/**
 * Edit page of preset
 *
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 */

require_once ('lib.php');
require_once(__DIR__ . '/class/preset_edit_form.php');

//try to get preset id for the form values
$presetid = optional_param('presetid', 0, PARAM_INT);

$context = context_system::instance();
$PAGE->set_context($context);

$manageanypresets = has_capability('block/paramtest:managepresets', $context);
if (!$manageanypresets) {
    require_capability('block/paramtest:managepresets', $context);
}

$adminpresets = new moodle_url('/admin/settings.php', array('section' => 'blocksettingparamtest'));

$PAGE->set_url('/blocks/paramtest/editpreset.php');
$PAGE->set_pagelayout('admin');

//Get existing presets
$presets = get_presets();

if($presetid) {
    $presetrecord = $presets[$presetid];
    $isadding = false;
} else {
    $isadding = true;
}
// Preset edit form class
$mform = new preset_edit_form($PAGE->url, $isadding, $presetid);

if(!empty($presetrecord)) {
    $mform->set_data($presetrecord);
}

// Post process
if ($mform->is_cancelled()) {
    redirect($adminpresets);
} else if ($data = $mform->get_data()) {
    $data->userid = $USER->id;
    if (!$manageanypresets) {
        $data->shared = 0;
    }
    if ($isadding) {
        // Need to start at 1, because the presetid cannot be 0 or it will not go through some conditions
        if(empty($presets)) {
            $presets[1] = $data;
        } else {
            $presets[] = $data;
        }
    } else {
        $presets[$presetid] = $data;
    }
    //encode the table in json and rewrite json file to save the presets

    set_presets($presets);

    redirect($adminpresets);
} else {
    if ($isadding) {
        $strtitle = get_string('addnewpreset', 'block_paramtest');
    } else {
        $strtitle = get_string('editapreset', 'block_paramtest');
    }

    $PAGE->set_title($strtitle);
    $PAGE->set_heading($strtitle);

    /*$PAGE->navbar->add(get_string('blocks'));
    $PAGE->navbar->add(get_string('pluginname', 'block_paramtest'));
    $PAGE->navbar->add(get_string('managepresets', 'block_paramtest'), $adminpresets);
    $PAGE->navbar->add($strtitle);*/

    echo $OUTPUT->header();
    echo $OUTPUT->heading($strtitle, 2);

    $mform->display();

    echo $OUTPUT->footer();
}

