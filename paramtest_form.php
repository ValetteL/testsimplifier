<?php
/**
 * Test (quiz) Form
 *
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/quiz/locallib.php');
require_once('lib.php');

/**
 * Test Form class
 *
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 */
class paramtest_form extends moodleform {

    public static $datefieldoptions = array('optional' => true);

    protected $data;
    protected $course;

    /**
     * paramtest_form constructor.
     * @param $data
     * @param $course
     * @inheritDoc
     */
    public function __construct($data, $course) {
        $this->data = $data;
        $this->course = $course;
        parent::__construct(null, $data);
    }

    /**
     * Test form definition
     *
     * @throws coding_exception
     * @throws dml_exception
     * @throws moodle_exception
     * @inheritDoc
     */
    public function definition() {
        global $COURSE, $CFG, $DB, $PAGE;

        $PAGE->requires->jquery();
        $PAGE->requires->js('/blocks/paramtest/script/presets.js', true);

        $quizconfig = get_config('quiz');
        $mform = $this->_form;

        // Get all presets
        $presets = get_presets();

        // Dropdown of presets
        $presetsnames = array();
        foreach ($presets as $key => $preset)
        {
            $presetsnames[$key] = $preset->name;
        }

        $presetsnames[0] = get_string('chooseapreset', 'block_paramtest');

        $select = $mform->addElement('select', 'presets', get_string('presets', 'block_paramtest'), $presetsnames);
        $select->setSelected(0);
        $mform->addRule('presets', get_string('error', 'block_paramtest'), 'nonzero', null, 'client');


        foreach ($presets as $key => $preset) {
            // Moodle bug on static text, we must use a group to handle hideif on static text
            $group = [];
            $group[] =& $mform->createElement('static', 'desc' . $key, ' ', $preset->description['text']);
            $mform->addGroup($group, 'description' . $key, '', ' ', false);
            $mform->hideIf('description' . $key, 'presets', 'neq', $key);
        }

        // Test Name
        $mform->addElement('text', 'name', get_string('name'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->hideIf('name', 'presets', 'eq', 0);

        // Test Description
        $group = [];
        $group[] =& $mform->createElement('editor', 'introeditor', '', array('rows' => '10'),
            array('maxfiles' => EDITOR_UNLIMITED_FILES, 'noclean' => true, 'context' => context_course::instance($this->data->course), 'subdirs' => true));
        $mform->setType('introeditor', PARAM_CLEANHTML);
        $mform->addGroup($group, 'intro', get_string('introduction', 'quiz'), array(' '), false);
        $mform->hideIf('intro', 'presets', 'eq', 0);

        // Section test
        $sections = array();
        foreach(get_fast_modinfo($this->course)->get_section_info_all() as $section) {
            $sections[$section->section] =  $section->name !== null ?
                $section->name :
                'Section ' . ($section->section == 0 ?
                    'Entête' :
                    $section->section);
        }
        $mform->addElement('select', 'section', get_string('sections'), $sections);
        $mform->hideIf('section', 'presets', 'eq', 0);

        $mform->addElement('date_time_selector', 'timeopen', get_string('opendate', 'block_paramtest'),
            self::$datefieldoptions);
        $mform->hideIf('timeopen', 'presets', 'eq', 0);

        $mform->addElement('date_time_selector', 'timeclose', get_string('closedate', 'block_paramtest'),
            self::$datefieldoptions);
        $mform->hideIf('timeclose', 'presets', 'eq', 0);

        // Number of attempts.
        $attemptoptions = array('0' => get_string('unlimited'));
        for ($i = 1; $i <= QUIZ_MAX_ATTEMPT_OPTION; $i++) {
            $attemptoptions[$i] = $i;
        }
        $mform->addElement('select', 'attempts', get_string('attemptsallowed', 'quiz'),
            $attemptoptions);
        $mform->setAdvanced('attempts', $quizconfig->attempts_adv);
        $mform->setDefault('attempts', $quizconfig->attempts);
        $mform->hideIf('attempts', 'presets', 'eq', 0);

        // Timelimit / duration
        $mform->addElement('duration', 'timelimit', get_string('duration', 'block_paramtest'));
        $mform->hideIf('timelimit', 'presets', 'eq', 0);

        // Quiz password
        $mform->addElement('passwordunmask', 'quizpassword', get_string('quizpassword', 'block_paramtest'));
        $mform->setType('quizpassword', PARAM_TEXT);
        $mform->addHelpButton('quizpassword', 'requirepassword', 'quiz');
        $mform->setAdvanced('quizpassword', $quizconfig->password_adv);
        $mform->setDefault('quizpassword', $quizconfig->password);
        $mform->hideIf('quizpassword', 'presets', 'eq', 0);

        // Hidden quizid
        $mform->addElement('hidden', 'grade', '10'); // par défaut -> 10
        $mform->setType('grade', PARAM_INT);
        $mform->addElement('hidden', 'showdescription', '0');
        $mform->setType('showdescription', PARAM_TEXT);
        $mform->addElement('hidden', 'courseid', $this->data->course);
        $mform->setType('courseid', PARAM_INT);
        $mform->addElement('hidden', 'quizid', $this->data->id);
        $mform->setType('quizid', PARAM_INT);
        $mform->addElement('hidden', 'gradepass', 0);
        $mform->setType('gradepass', PARAM_FLOAT);
        $mform->addElement('hidden', 'modulename', 'quiz');
        $mform->setType('modulename', PARAM_TEXT);
        $mform->addElement('hidden', 'coursemodule', $this->data->coursemodule);
        $mform->setType('coursemodule', PARAM_INT);
        $mform->addElement('hidden', 'graceperiod', '0');
        $mform->setType('graceperiod', PARAM_INT);
        $mform->addElement('hidden', 'visibleoncoursepage', '1');
        $mform->setType('visibleoncoursepage', PARAM_INT);
        $mform->addElement('hidden', 'grademethod', '1');
        $mform->setType('grademethod', PARAM_INT);
        $mform->addElement('hidden', 'cmidnumber', '');
        $mform->setType('cmidnumber', PARAM_TEXT);
        $mform->addElement('hidden', 'module', $this->data->module);
        $mform->setType('module', PARAM_INT);
        $mform->addElement('hidden', 'shuffleanswers', '1');
        $mform->setType('shuffleanswers', PARAM_INT);
        $mform->addElement('hidden', 'decimalpoints', '2');
        $mform->setType('decimalpoints', PARAM_INT);
        $mform->addElement('hidden', 'questiondecimalpoints', '-1');
        $mform->setType('questiondecimalpoints', PARAM_INT);
        $mform->addElement('hidden', 'questionsperpage', '1');
        $mform->setType('questionsperpage', PARAM_INT);
        $mform->addElement('hidden', 'visible', '1');
        $mform->setType('visible', PARAM_INT);
        $mform->addElement('hidden', 'browsersecurity', '-');
        $mform->setType('browsersecurity', PARAM_TEXT);
        $mform->addElement('hidden', 'update');
        $mform->setType('update', PARAM_RAW);
        $behaviour = $mform->addElement('hidden', 'preferredbehaviour');
        if(!$this->data->id) {
            $behaviour->setValue('deferredfeedback');
        }
        $mform->setType('preferredbehaviour', PARAM_TEXT);

        // hidden review fields
        /*$prefixesreview = ["during", "immediately", "open", "closed"];
        $prefixes = ["attempt", "correctness", "marks", "specificfeedback", "generalfeedback", "rightanswer", "overallfeedback"];
        foreach ($prefixesreview as $prefixreview) {
            $group = array();
            foreach ($prefixes as $prefix) {
                $mform->addElement('hidden', $prefix . $prefixreview, '1');
                $mform->setType($prefix . $prefixreview, PARAM_RAW);
                foreach ($presets as $key => $value) {
                    $reviewchk = "review".$prefixreview.'chk';
                    if ($value->$reviewchk === 0) {
                        $mform->disabledIf($prefix . $prefixreview, 'presets', 'eq', $key);
                    }
                }
            }
        }*/

        // Loop to hide fields according to the presets
        foreach($presets as $key => $preset) {
            if($preset->overduehandlingchk === 1 || !$this->data->id) {
                $mform->addElement('hidden', 'overduehandling', 'autosubmit');
                $mform->setType('overduehandling', PARAM_TEXT);
            }

            if($preset->sectionschk === 0) {
                $mform->hideIf('section', 'presets', 'eq', $key);
            }

            if($preset->timelimitchk === 0) {
                $mform->disabledIf('timelimit', 'presets', 'eq', $key);
                $mform->hideIf('timelimit', 'presets', 'eq', $key);
            }

            if($preset->timeopenchk === 0) {
                $mform->hideIf('timeopen', 'presets', 'eq', $key);
            }

            if($preset->timeclosechk === 0) {
                $mform->hideIf('timeclose', 'presets', 'eq', $key);
            }

            if($preset->overduehandlingchk === 0) {
                $mform->hideIf('overduehandling', 'presets', 'eq', $key);
            }

            if($preset->quizpasswordchk === 0) {
                $mform->hideIf('quizpassword', 'presets', 'eq', $key);
            }

            if($preset->subnetchk === 1) {
                $adresses = explode("\r\n", $preset->subnet);
                $group = array();
                foreach ($adresses as $address) {
                    $expaddr = explode("|", $address);
                    $group[] =& $mform->createElement('advcheckbox', $expaddr[1], '', $expaddr[0], array('name' => $expaddr[0], 'group' => 1), $expaddr[1]);
                    $mform->setType('subnet', PARAM_TEXT);
                }
                $mform->addGroup($group, 'subnet', get_string('subnet', 'block_paramtest'), null, false);
                $mform->hideIf('subnet', 'presets', 'neq', $key);
            }
        }

        // Review options.
        $this->add_review_options_group($mform, $quizconfig, 'during',
            mod_quiz_display_options::DURING, true);
        $this->add_review_options_group($mform, $quizconfig, 'immediately',
            mod_quiz_display_options::IMMEDIATELY_AFTER);
        $this->add_review_options_group($mform, $quizconfig, 'open',
            mod_quiz_display_options::LATER_WHILE_OPEN);
        $this->add_review_options_group($mform, $quizconfig, 'closed',
            mod_quiz_display_options::AFTER_CLOSE);

        $this->add_action_buttons();
    }

    /**
     * Validation methodw
     *
     * @param array $data
     * @param array $files
     * @return array
     * @throws coding_exception
     */
    function validation($data, $files) {
        $errors = parent::validation($data, $files);

        // Check open and close times are consistent.
        if ($data['timeopen'] != 0 && $data['timeclose'] != 0
            && $data['timeclose'] < $data['timeopen']) {
            $errors['timeclose'] = get_string('closebeforeopen', 'quiz');
        }

        return $errors;
    }

    function add_action_buttons($cancel = true, $submitlabel = null, $submit2label = null, $submit3label = null)
    {
        if (is_null($submitlabel)) {
            $submitlabel = get_string('savechangesanddisplay');
        }
        if (is_null($submit2label)) {
            $submit2label = get_string('savechangesandreturntocourse');
        }
        if (is_null($submit3label)) {
            $submit3label = get_string('savechangesanddisplaybaseform', 'block_paramtest');
        }

        $mform = $this->_form;

        // elements in a row need a group
        $buttonarray = array();

        // Label for the submit button to return to the course.
        // Ignore this button in single activity format because it is confusing.
        if ($submit3label !== false) {
            $buttonarray[] = &$mform->createElement('submit', 'savechangesanddisplaybaseform', $submit3label);
        }
        if ($submit2label !== false && course_get_format($this->data->course)->has_view_page()) {
            $buttonarray[] = &$mform->createElement('submit', 'savechangesandreturntocourse', $submit2label);
        }

        if ($submitlabel !== false) {
            $buttonarray[] = &$mform->createElement('submit', 'savechangesanddisplay', $submitlabel);
        }

        if ($cancel) {
            $buttonarray[] = &$mform->createElement('cancel');
        }

        $mform->addGroup($buttonarray, 'buttonar', '', array(''), false);
        $mform->setType('buttonar', PARAM_RAW);
        $mform->closeHeaderBefore('buttonar');
    }

    protected function add_review_options_group($mform, $quizconfig, $whenname,
                                                $when, $withhelp = false) {
        global $OUTPUT;

        $reviewfields = array(
            'attempt'          => array('theattempt', 'quiz'),
            'correctness'      => array('whethercorrect', 'question'),
            'marks'            => array('marks', 'quiz'),
            'specificfeedback' => array('specificfeedback', 'question'),
            'generalfeedback'  => array('generalfeedback', 'question'),
            'rightanswer'      => array('rightanswer', 'question'),
            'overallfeedback'  => array('reviewoverallfeedback', 'quiz'),
        );

        $presets = get_presets();

        $group = array();
        foreach ($reviewfields as $field => $string) {
            list($identifier, $component) = $string;
            $label = get_string($identifier, $component);

            $group[] = $mform->createElement('checkbox', $field . $whenname, '', $label);

            foreach ($presets as $key => $value) {
                $reviewchk = "review".$whenname.'chk';
                if ($value->$reviewchk === 0) {
                    $mform->disabledIf($field . $whenname, 'presets', 'eq', $key);
                }
            }
        }
        $mform->addGroup($group, $whenname . 'optionsgrp',
            get_string('review' . $whenname, 'quiz'), null, false);

        foreach ($reviewfields as $field => $notused) {
            $cfgfield = 'review' . $field;
            if ($quizconfig->$cfgfield & $when) {
                $mform->setDefault($field . $whenname, 1);
            } else {
                $mform->setDefault($field . $whenname, 0);
            }
        }
    }

    function data_preprocessing(&$default_values) {
        //TODO : if necessary, can add process to setData()
    }

    function set_data($default_values) {
        if (is_object($default_values)) {
            $default_values = (array)$default_values;
        }

        $this->data_preprocessing($default_values);
        parent::set_data($default_values);
    }

    public function data_postprocessing($data) {
        //check if it's needed to subnet reformat
        if(!empty($data->subnet)) {
            $subnet = '';
            foreach($data->subnet as $address) {
                if($address != '') {
                    $subnet .= $address . ',';
                }
            }
            $data->subnet = $subnet;
        }
    }

    public function get_data() {
        $data = parent::get_data();
        if ($data) {
            // Convert the grade pass value - we may be using a language which uses commas,
            // rather than decimal points, in numbers. These need to be converted so that
            // they can be added to the DB.
            if (isset($data->gradepass)) {
                $data->gradepass = unformat_float($data->gradepass);
            }

            $this->data_postprocessing($data);
        }
        return $data;
    }
}