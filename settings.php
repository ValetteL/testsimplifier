<?php
/**
 * Block paramtest admin settings.
 *
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

    require_once(__DIR__ . '/../../config.php');
    require_once($CFG->libdir . '/adminlib.php');
    require_once($CFG->libdir . '/tablelib.php');
    require_once($CFG->dirroot . '/blocks/paramtest/lib.php');

    $returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
    $deletepresetid = optional_param('deletepresetid', 0, PARAM_INT);

    $context = context_system::instance();

    // Verify plugin capabilities and authentification
    require_login();
    require_capability('block/paramtest:managepresets', $context);

    $baseurl = new moodle_url('/admin/settings.php', array('section' => 'blocksettingparamtest'));

    // Get the list of presets.
    $presets = get_presets();

    // Process any actions
    if ($deletepresetid && confirm_sesskey()) {
        unset($presets[$deletepresetid]);
        array_values($presets);
        set_presets($presets);
        redirect($PAGE->url, get_string('presetsdeleted', 'block_paramtest'));
    }

    $strmanage = get_string('managepresets', 'block_paramtest');

    $str = '';

    $table = new html_table();
    $table->head = array(get_string('preset', 'block_paramtest'), get_string('actions', 'moodle'));

    foreach ($presets as $presetid => $preset) {
        $presettitle = $preset->name;

        $viewlink = html_writer::link($CFG->wwwroot . '/blocks/paramtest/viewpreset.php?presetid=' . $presetid, $presettitle);

        $presetinfo = '<div class="title">' . $presettitle . '</div>';

        $editurl = new moodle_url('/blocks/paramtest/editpreset.php', array("presetid" => $presetid));
        $editaction = $OUTPUT->action_icon($editurl, new pix_icon('t/edit', get_string('edit')));

        $deleteurl = new moodle_url('/admin/settings.php', array('section' => 'blocksettingparamtest', 'deletepresetid' => $presetid, 'sesskey' => sesskey()));
        $deleteicon = new pix_icon('t/delete', get_string('delete'));
        $deleteaction = $OUTPUT->action_icon($deleteurl, $deleteicon, new confirm_action(get_string('deletepresetconfirm', 'block_paramtest')));

        $preseticons = $editaction . ' ' . $deleteaction;

        $table->data[] = (array($presetinfo, $preseticons));
    }

    $str .= html_writer::table($table);

    $url = new moodle_url('/blocks/paramtest/editpreset.php');
    /*$OUTPUT->single_button($url, get_string('addnewpreset', 'block_paramtest'), 'get')*/
    $str .= '<div class="actionbuttons">' . html_writer::link($url, get_string('addnewpreset', 'block_paramtest')) . '</div>';

    $settings->add(new admin_setting_heading('managepreset', get_string('managepresets', 'block_paramtest'), $str));
}