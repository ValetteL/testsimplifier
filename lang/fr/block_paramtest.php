<?php

/**
 * Language file
 *
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 */

defined('MOODLE_INTERNAL') || die();

$string['headerconfig'] = 'Configuration';
$string['descconfig'] = 'Première rubrique de configuration';
$string['labelallowhtml'] = 'Autoriser le HTML';
$string['descallowhtml'] = 'autoriser le html';
$string['timecreated'] = 'Date de création';
$string['testname'] = 'Nom du test';
$string['defaulttitle'] = 'Bloc de paramétrage de tests';
$string['pluginname'] = 'Bloc de paramétrage de tests';
$string['paramtest'] = 'Test Parameter';
$string['paramtest:addinstance'] = 'Ajoute un nouveau Bloc de paramétrage de tests';
$string['paramtest:myaddinstance'] = 'Ajoute un nouveau Bloc de paramétrage de tests sur ma page Moodle';
$string['attempts'] = 'Tentatives';
$string['questionsrandomised'] = 'Questions mélangées';
$string['answersrandomised'] = 'Réponses mélangées';
$string['opendate'] = 'Date d\'ouverture';
$string['closedate'] = 'Date de fermeture';
$string['duration'] = 'Durée du test';
$string['presets'] = 'Préreglages';
$string['passinggrade'] = 'Note pour passer';
$string['savechangesanddisplaybaseform'] = 'Sauvegarder et revenir au formulaire de Moodle';
$string['presetdeleted'] = 'Préreglage supprimé';
$string['presetadded'] = 'Préreglage ajouté';
$string['managepresets'] = 'Gestion des préreglages';
$string['preset'] = 'Préreglages';
$string['failedpreset'] = 'Télechargement du préreglage échoué - nouvel essai dans {$a}';
$string['deletepresetconfirm'] = 'Êtes vous sur de vouloir supprimer ce préreglage ?';
$string['addnewpreset'] = 'Ajouter un préreglage';
$string['presetsaddedit'] = 'Ajouter/Modifier un préreglage';
$string['editapreset'] = 'Edition d\'un préreglage';
$string['modifytestsection'] = 'Modification de la section du test';
$string['decimalpoints'] = 'Points décimaux de la note (par défaut : 2)';
$string['questiondecimalpoints'] = 'Points décimaux des questions (par défaut : comme la note)';
$string['overduehandling'] = 'Auto submission du formulaire à la fin d\'une tentative';
$string['shuffleanswers'] = 'Réponses mélangés';
$string['reviewattempt'] = 'Relecture de la tentative (par défaut : juste après, tant que le test est encore ouvert)';
$string['reviewduring'] = 'Pendant la tentative';
$string['reviewimmediatelyafter'] = 'Juste après la tentative';
$string['reviewlaterwhileopen'] = 'Tant que le test est encore ouvert';
$string['reviewafterclose'] = 'Après la fermeture du test';
$string['quizpassword'] = 'Mot de passe d\'accès';
$string['chooseapreset'] = 'Choisir un préreglage...';
$string['questionsperpage'] = 'Nombre de questions par page (par défaut : 0)';
$string['presetsdeleted'] = 'Préreglage supprimé';
$string['visible'] = 'Afficher les tests sur la page de cours';
$string['description'] = 'Description du préreglage';
$string['subnet'] = 'Adresse IP';
$string['subnet_help'] = 'A saisir sous la forme : "salle truc|10.0.0.0(retour à la ligne)"';
$string['libelleip'] = 'Libelle de l\'adresse';
$string['valueip'] = 'adresse IP';
$string['testlist'] = 'Liste des tests';
$string['addtest'] = 'Ajouter un test';
$string['browsersecurity'] = 'Sécurité du navigateur';
$string['error'] = 'Vous devez choisir un préreglage';
$string['showdescription'] = 'Afficher la description du test sur la page de cours';
