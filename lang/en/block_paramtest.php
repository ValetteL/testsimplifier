<?php

/**
 * Language file
 *
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 */

defined('MOODLE_INTERNAL') || die();

$string['headerconfig'] = 'Configuration';
$string['descconfig'] = 'First part of the configuration';
$string['labelallowhtml'] = 'allow the HTML';
$string['descallowhtml'] = 'allow the html';
$string['timecreated'] = 'Creation date';
$string['testname'] = 'Test name';
$string['defaulttitle'] = 'Test Parameter Block';
$string['pluginname'] = 'Test Parameter Block';
$string['paramtest'] = 'Test Parameter';
$string['paramtest:addinstance'] = 'Add a new test parameter block';
$string['paramtest:myaddinstance'] = 'Add a new test parameter block to the My Moodle page';
$string['attempts'] = 'Attempts';
$string['questionsrandomised'] = 'Questions randomised';
$string['answersrandomised'] = 'Answers randomised';
$string['opendate'] = 'Open date';
$string['closedate'] = 'Closure date';
$string['duration'] = 'Test duration';
$string['presets'] = 'Presets';
$string['passinggrade'] = 'Note to pass';
$string['savechangesanddisplaybaseform'] = 'Save Changes and Display Base Moodle Form';
$string['presetdeleted'] = 'News preset deleted';
$string['presetadded'] = 'News preset added';
$string['managepresets'] = 'Manage presets';
$string['preset'] = 'Preset';
$string['failedpreset'] = 'Preset failed to download - wil retry after {$a}';
$string['deletepresetconfirm'] = 'Are you sure you want to delete this preset ?';
$string['addnewpreset'] = 'Add a new preset';
$string['presetsaddedit'] = 'Add/Edit Presets';
$string['editapreset'] = 'Edit a preset';
$string['modifytestsection'] = 'Modify the test section';
$string['decimalpoints'] = 'Decimal points of the grade (default : 2)';
$string['questiondecimalpoints'] = 'Decimal points of the questions (default : same as the grade)';
$string['overduehandling'] = 'Autosubmit of an attempt at the end of the delay';
$string['shuffleanswers'] = 'Shuffle answers';
$string['reviewattempt'] = 'Attempt review (default : just after and later while open)';
$string['reviewduring'] = 'During the attempt';
$string['reviewimmediatelyafter'] = 'Just after the attempt';
$string['reviewlaterwhileopen'] = 'As long as the test stills open';
$string['reviewafterclose'] = 'After the close of the test';
$string['quizpassword'] = 'Access password';
$string['chooseapreset'] = 'Choose a preset...';
$string['questionsperpage'] = 'Number of question per page (default : 0)';
$string['presetsdeleted'] = 'Preset deleted';
$string['visible'] = 'Show the tests on course page';
$string['description'] = 'Preset\'s description';
$string['subnet'] = 'IP adress';
$string['subnet_help'] = 'The pattern should be : "room truc|10.0.0.0(back to line)"';
$string['libelleip'] = 'libelle name';
$string['valueip'] = 'IP adress';
$string['testlist'] = 'Tests list';
$string['addtest'] = 'Add a test';
$string['browsersecurity'] = 'Browser security';
$string['error'] = 'You need to choose a preset';
$string['showdescription'] = 'Show description of the test on course page';