<?php
/**
 * Edit page of test (quiz)
 *
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 */

require_once("../../config.php");
require_once($CFG->dirroot ."/course/lib.php");
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/gradelib.php');
require_once($CFG->libdir.'/completionlib.php');
require_once($CFG->libdir.'/plagiarismlib.php');
require_once($CFG->dirroot . '/course/modlib.php');

// Try to get a quizid or a courseid, depends if it's a test update or a creation
$quizid = optional_param('quizid', 0, PARAM_INT);
$courseid = optional_param('courseid', 0, PARAM_INT);
$update = optional_param('update', 'a', PARAM_RAW);

$url = new moodle_url('/blocks/paramtest/edittest.php');

// Update process
if ($quizid !== 0) {
    /*var_dump('coucou');
    die();*/
    $url->param('quizid', $quizid);

    // Select the "Edit settings" from navigation.
    // navigation_node::override_active_url($url);

    $quiz = $DB->get_record('quiz', array('id' => $quizid), '*', MUST_EXIST);

    // Check the course exists.
    $course = $DB->get_record('course', array('id' => $quiz->course), '*', MUST_EXIST);

    // Check the course module exists.
    $cm = $DB->get_record('course_modules', array('module' => '16', 'instance' => $quizid), '*', MUST_EXIST);

    list($cm, $context, $module, $data, $cw) = get_moduleinfo_data($cm, $course);
    $cm->name = $data->name;
    $cm->modname = $data->modulename;

    // require_login
    require_login($course); // needed to setup proper $COURSE

    $navbaraddition = null;
// Creation process
} else if ($courseid !== 0) {

    $url->param('courseid', $courseid);

    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);

    // There is no page for this in the navigation. The closest we'll have is the course section.
    // If the course section isn't displayed on the navigation this will fall back to the course which
    // will be the closest match we have.
    // navigation_node::override_active_url(course_get_url($course, $section));

    list($module, $context, $cw, $cm, $data) = prepare_new_moduleinfo_data($course, 'quiz', '0');

    $sectionname = get_section_name($course, $cw);
    $fullmodulename = get_string('modulename', $module->name);

    if ($data->section && $course->format != 'site') {
        $heading = new stdClass();
        $heading->what = $fullmodulename;
        $heading->to   = $sectionname;
        $pageheading = get_string('addinganewto', 'moodle', $heading);
    } else {
        $pageheading = get_string('addinganew', 'moodle', $fullmodulename);
    }

    require_login($course);
    $navbaraddition = $pageheading;
} else {
    require_login();
    print_error('invalidaction');
}

$PAGE->set_url($url);

$PAGE->set_pagelayout('course');

require_once('./paramtest_form.php');

// Test form class
$mform = new paramtest_form($data, $course);

if($update == 'a') {
    $mform->set_data($data);
}

// Post process
if ($mform->is_cancelled()) {
    redirect("$CFG->wwwroot/blocks/paramtest/testlist.php?courseid=" . $course->id);
// fromform contains post data infos
} else if ($fromform = $mform->get_data()) {
    // Update condition
    if ($fromform->quizid != 0) {
        list($cm, $fromform) = update_moduleinfo($cm, $fromform, $course, $mform);
        if(get_fast_modinfo($course)->get_cm($cm->id)->get_section_info() !== $fromform->section) {
            $modcontext = context_module::instance($cm->id);
            require_capability('moodle/course:manageactivities', $modcontext);
            if (!$section = $DB->get_record('course_sections', array('course' => $course->id, 'section' => $fromform->section))) {
                throw new moodle_exception('AJAX commands.php: Bad section ID '. $fromform->section);
            }
            moveto_module($cm, $section);
        }
    // Add condition
    } else if ($fromform->courseid != 0) {
        $fromform = add_moduleinfo($fromform, $course, $mform);
    } else {
        print_error('invaliddata');
    }

    if (isset($fromform->savechangesanddisplay)) {
        redirect("$CFG->wwwroot/mod/quiz/view.php?id=$fromform->coursemodule&forceview=1");
    } else if(isset($fromform->savechangesandreturntocourse)) {
        redirect("$CFG->wwwroot/course/view.php?id=" . $fromform->course);
    } else if(isset($fromform->savechangesanddisplaybaseform)) {
        redirect("$CFG->wwwroot/course/modedit.php?update=$fromform->coursemodule&return=1");
    }
    exit;
} else {
    $PAGE->set_heading($course->fullname);
    $PAGE->set_title(isset($quiz) ? $quiz->name : $course->fullname);
    $PAGE->set_cacheable(false);

    if (isset($navbaraddition)) {
        $PAGE->navbar->add($navbaraddition);
    }

    echo $OUTPUT->header();

    $mform->display();

    echo $OUTPUT->footer();
}
