<?php
/**
 * List of presets page
 *
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 */

require_once(__DIR__ . '/../../config.php');

//require_once($CFG->libdir.'/adminlib.php');

//admin_externalpage_setup('blocks_paramtest');

require_once(__DIR__ . '/../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir . '/tablelib.php');
require_once ('lib.php');

$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
$deletepresetid = optional_param('deletepresetid', 0, PARAM_INT);

$context = context_system::instance();

// Verify plugin capabilities and authentification
require_login();
require_capability('block/paramtest:managepresets', $context);

$PAGE->set_context($context);

$baseurl = new moodle_url('/blocks/paramtest/managepresets.php');
$PAGE->set_url($baseurl);

// Get the list of presets.
$presets = get_presets();

// Process any actions
if ($deletepresetid && confirm_sesskey()) {
    unset($presets[$deletepresetid]);
    array_values($presets);
    set_presets($presets);
    redirect($PAGE->url, get_string('presetsdeleted', 'block_paramtest'));
}

$strmanage = get_string('managepresets', 'block_paramtest');

$PAGE->set_pagelayout('standard');
$PAGE->set_title($strmanage);
$PAGE->set_heading($strmanage);

$PAGE->navbar->add(get_string('blocks'));
$PAGE->navbar->add(get_string('pluginname', 'block_paramtest'));
$PAGE->navbar->add(get_string('managepresets', 'block_paramtest'));

echo $OUTPUT->header();

$table = new flexible_table('paramtest-display-presets');

$table->define_columns(array('preset', 'actions'));
$table->define_headers(array(get_string('preset', 'block_paramtest'), get_string('actions', 'moodle')));
$table->define_baseurl($baseurl);

$table->set_attribute('cellspacing', '0');
$table->set_attribute('id', 'paramtestpresets');
$table->set_attribute('class', 'generaltable generalbox');
$table->column_class('presets', 'presets');
$table->column_class('actions', 'actions');

// Setup table data with presets
$table->setup();
foreach ($presets as $presetid => $preset) {
    $presettitle = $preset->name;

    $viewlink = html_writer::link($CFG->wwwroot . '/blocks/paramtest/viewpreset.php?presetid=' . $presetid, $presettitle);

    $presetinfo = '<div class="title">' . $presettitle . '</div>';

    $editurl = new moodle_url('/blocks/paramtest/editpreset.php?presetid=' . $presetid);
    $editaction = $OUTPUT->action_icon($editurl, new pix_icon('t/edit', get_string('edit')));

    $deleteurl = new moodle_url('/blocks/paramtest/managepresets.php?deletepresetid=' . $presetid . '&sesskey=' . sesskey());
    $deleteicon = new pix_icon('t/delete', get_string('delete'));
    $deleteaction = $OUTPUT->action_icon($deleteurl, $deleteicon, new confirm_action(get_string('deletepresetconfirm', 'block_paramtest')));

    $preseticons = $editaction . ' ' . $deleteaction;

    $table->add_data(array($presetinfo, $preseticons));
}

$table->print_html();

$url = $CFG->wwwroot . '/blocks/paramtest/editpreset.php';
echo '<div class="actionbuttons">' . $OUTPUT->single_button($url, get_string('addnewpreset', 'block_paramtest'), 'get') . '</div>';

if ($returnurl) {
    echo '<div class="backlink">' . html_writer::link($returnurl, get_string('back')) . '</div>';
}

echo $OUTPUT->footer();
