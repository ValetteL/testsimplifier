<?php

/**
 * Block paramtest
 *
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Block paramtest Class
 *
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 */
class block_paramtest extends block_list
{
    public function init()
    {
        $this->title = get_string('pluginname', 'block_paramtest');
    }

    public function has_config()
    {
        return true;
    }

    public function get_content()
    {
        global $CFG, $OUTPUT, $PAGE;

        //$PAGE->requires->css(new moodle_url("/blocks/paramtest/style.css");

        $iconlist = $OUTPUT->pix_icon('list', get_string('testlist', 'block_paramtest'), 'block_paramtest');
        $iconadd = $OUTPUT->pix_icon('add', get_string('addtest', 'block_paramtest'), 'block_paramtest');

        if ($this->content !== null) {
            return $this->content;
        }

        $courseid = optional_param('id', 0, PARAM_INT); // Course id
        if ($courseid == 0) {
            $courseid = optional_param('courseid', 0, PARAM_INT); // Course id
        }
        $this->content         = new stdClass;
        $this->content->items  = array();
        $this->content->icons  = array();

        //Link to test list or add a test
        $this->content->items[]="<a href=\"$CFG->wwwroot/blocks/paramtest/testlist.php?courseid=$courseid\">".
            $iconlist . get_string('testlist', 'block_paramtest') . "</a>";
        $this->content->items[]="<a href=\"$CFG->wwwroot/blocks/paramtest/edittest.php?courseid=$courseid\">".
            $iconadd . get_string('addtest', 'block_paramtest') . "</a>";

        return $this->content;
    }

    public function specialization()
    {
        if (isset($this->config)) {
            if (empty($this->config->title)) {
                $this->title = get_string('defaulttitle', 'block_paramtest');
            } else {
                $this->title = $this->config->title;
            }

            if (empty($this->config->text)) {
                $this->config->text = get_string('defaulttext', 'block_paramtest');
            }
        }
    }

    public function instance_config_save($data, $nolongerused = false)
    {
        if(get_config('paramtest', 'Allow_HTML') == '1') {
            $data->text = strip_tags($data->text);
        }

        // And now forward to the default implementation defined in the parent class
        return parent::instance_config_save($data,$nolongerused);
    }

    public function applicable_formats()
    {
        return array(
            'site-index' => false,
            'course-view' => true,
            'mod' => false,
            'mod-quiz' => false
        );
    }

}