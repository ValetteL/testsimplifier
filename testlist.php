<?php
/**
 * Test list page
 *
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 */

require_once("../../config.php");
require_once($CFG->dirroot . '/course/lib.php');

// Get with method GET or POST courseid
$courseid = optional_param('courseid', 0, PARAM_INT);

$params = array();
if (!empty($courseid)) {
    $params = array('id' => $courseid);
} else {
    print_error('unspecifycourseid', 'error');
}

$context = context_course::instance($courseid, MUST_EXIST);

$PAGE->set_context($context);
$PAGE->set_title(get_string('testlist', 'block_paramtest'));

// Get the course and the tests associated
$course = $DB->get_record('course', $params, '*', MUST_EXIST);
$quizrecords = $DB->get_records('quiz', ['course' => $courseid]);

// Usefull for redirection
$urlparams = array('courseid' => $course->id);
$PAGE->set_url('/blocks/paramtest/testlist.php', $urlparams);

// Verify if the user is authentified and if the course access is authorized,
// if yes, so it will display the navigationlink under the header of the course
require_login($course);

// display the layout associated at the course (the blocks module)
$PAGE->set_pagelayout('course');

$course->format = course_get_format($course)->get_format();
$PAGE->set_pagetype('course-view-' . $course->format);

// show the header of the course (Title)
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();

// Setup the list of the tests associated at the course
$table = new html_table();
$table->head  = array(
    get_string('testname', 'block_paramtest'),
    get_string('attempts', 'block_paramtest'),
    get_string('answersrandomised', 'block_paramtest'),
    get_string('opendate', 'block_paramtest'),
    get_string('closedate', 'block_paramtest'),
    get_string('browsersecurity', 'block_paramtest'),
    ''
);

foreach ($quizrecords as $quiz)
{
    $row = new html_table_row();
    $row->cells[] = html_writer::link(new moodle_url('/mod/quiz/view.php?q=' . $quiz->id), $quiz->name);
    $row->cells[] = $quiz->attempts == 0 ? 'Illimité' : $quiz->attempts;
    $row->cells[] = $quiz->shuffleanswers == 1 ? 'Oui' : 'Non';
    $row->cells[] = $quiz->timeopen == 0 ? 'Pas de restriction' : date('d/m/Y H:i:s', $quiz->timeopen);
    $row->cells[] = $quiz->timeclose == 0 ? 'Pas de restriction' : date('d/m/Y H:i:s', $quiz->timeclose);
    $row->cells[] = $quiz->browsersecurity == '-' ? 'Non' : 'Oui';
    $row->cells[] = html_writer::tag('a',
        html_writer::tag('i', '', array(
            'class' => 'icon fa fa-pencil fa-fw',
            'title' => 'Modifier le test',
        )),
        array(
            'href' => new moodle_url('/blocks/paramtest/edittest.php?quizid=' . $quiz->id),
        ));
    $table->data[] = $row;
}

// display the table of tests
echo html_writer::table($table);

// display the footer
echo $OUTPUT->footer();