<?php

/**
 * Class preset
 *
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 * @author Louis Valette <louis.valette@oniris-nantes.fr>
 */

class preset
{
    public $name;
    public $description;
    public $attempts;
    public $preferredbehaviour;
    public $sectionschk;
    public $timelimitchk;
    public $timeopenchk;
    public $timeclosechk;
    public $visible;
    public $showdescription;
    public $overduehandlingchk;
    public $shuffleanswers;
    public $questionsperpagechk;
    public $questionsperpage;
    public $reviewduringchk;
    public $reviewimmediatelychk;
    public $reviewopenchk;
    public $reviewclosedchk;
    public $browsersecurity;
    public $quizpasswordchk;
    public $subnetchk;
    public $subnet;

    /**
     * preset constructor.
     */
    public function __construct($data)
    {
        foreach ($data as $key => $value){
            if(property_exists($this, $key))
            {
                $this->$key = $value;
            }
        }
    }
}