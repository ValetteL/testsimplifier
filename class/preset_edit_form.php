<?php

/**
* Preset edit form
* @package block_paramtest
* @copyright 2020 Louis Valette - ONIRIS
*/
require_once(__DIR__ . '/../../../config.php');
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir .'/simplepie/moodle_simplepie.php');
require_once($CFG->dirroot . '/mod/quiz/locallib.php');

/**
 * Preset edit form class
 * @package block_paramtest
 * @copyright 2020 Louis Valette - ONIRIS
 */
class preset_edit_form extends moodleform {
    protected $isadding;
    protected $presetid;
    protected $title = '';
    protected $description = '';

    /**
     * preset_edit_form constructor.
     * @param $actionurl
     * @param $isadding
     * @param null $presetid
     */
    function __construct($actionurl, $isadding, $presetid = null) {
        $this->isadding = $isadding;
        $this->presetid = $presetid;
        parent::__construct($actionurl);
    }

    /**
     * Form definition
     *
     * @return void
     */
    function definition() {
        global $PAGE;

        $mform =& $this->_form;

        // Then show the fields about where this block appears.
        $mform->addElement('header', 'preseteditheader', get_string('preset', 'block_paramtest'));

        //Preset Name
        $mform->addElement('text', 'name', get_string('name'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $mform->addElement('editor', 'description', get_string('description', 'block_paramtest'), array('rows' => 10)); // , 'columns' => 10
        $mform->setType('description', PARAM_CLEANHTML);

        // List of fields of the form of a preset
        $mform->addElement('advcheckbox', 'sectionschk', get_string('modifytestsection', 'block_paramtest'), ' ');
        $mform->setType('sectionschk', PARAM_INT);

        // Time limit of an attempt checkbox, if checked this will show the field in the test preset form
        $mform->addElement('advcheckbox', 'timelimitchk', get_string('duration', 'block_paramtest'), ' ');
        $mform->setType('timelimitchk', PARAM_INT);

        // Time open and close limit checkbox to show or not the field in test preset form
        $mform->addElement('advcheckbox', 'timeopenchk', get_string('opendate', 'block_paramtest'), ' ');
        $mform->setType('timeopenchk', PARAM_INT);
        $mform->addElement('advcheckbox', 'timeclosechk', get_string('closedate', 'block_paramtest'), ' ');
        $mform->setType('timeclosechk', PARAM_INT);

        // Checkbox to show the test on the course page, by default, checked
        $mform->addElement('modvisible', 'visible', get_string('visible', 'block_paramtest', null));
        $mform->setType('visible', PARAM_INT);

        // Checkbox to show or not the test description
        $mform->addElement('advcheckbox', 'showdescription', get_string('showdescription', 'block_paramtest'), ' ');

        // OverdueHandling checkbox, if checked test's used with the preset will be on 'autosubmit'
        $mform->addElement('advcheckbox', 'overduehandlingchk', get_string('overduehandling', 'block_paramtest'), ' ');
        $mform->setType('overduehandlingchk', PARAM_INT);

        // Answer shuffle checkbox
        $mform->addElement('advcheckbox', 'shuffleanswers', get_string('shuffleanswers', 'block_paramtest'), ' '); // value -> '1'
        $mform->setType('shuffleanswers', PARAM_INT);

        // Number of question per page (by default 1) checkbox, and if checked text to enter the preset number
        $mform->addElement('advcheckbox', 'questionsperpagechk', get_string('questionsperpage', 'block_paramtest'), ' '); // voir la valeur par défaut sur le formulaire Moodle
        $mform->setType('questionsperpagechk', PARAM_INT);
        $mform->addElement('text', 'questionsperpage', '', array('value' => '0'));
        $mform->setType('questionsperpage', PARAM_INT);
        $mform->hideIf('questionsperpage', 'questionsperpagechk', 'notchecked');

        $group = array();
        $group[] = $mform->createElement('advcheckbox', 'reviewduringchk', '', get_string('reviewduring', 'block_paramtest'));
        $mform->setType('reviewduringchk', PARAM_INT);
        $group[] = $mform->createElement('advcheckbox', 'reviewimmediatelychk', '', get_string('reviewimmediatelyafter', 'block_paramtest'));
        $mform->setType('reviewimmediatelychk', PARAM_INT);
        $group[] = $mform->createElement('advcheckbox', 'reviewopenchk', '', get_string('reviewlaterwhileopen', 'block_paramtest'));
        $mform->setType('reviewopenchk', PARAM_INT);
        $group[] = $mform->createElement('advcheckbox', 'reviewclosedchk', '', get_string('reviewafterclose', 'block_paramtest'));
        $mform->setType('reviewclosedchk', PARAM_INT);
        $mform->addGroup($group, 'reviewattempt', get_string('reviewattempt', 'block_paramtest'), null, false);

        // Browser security choices.
        $mform->addElement('select', 'browsersecurity', get_string('browsersecurity', 'block_paramtest'),
            quiz_access_manager::get_browser_security_choices());
        $mform->setType('browsersecurity', PARAM_TEXT);

        $mform->addElement('advcheckbox', 'quizpasswordchk', get_string('quizpassword', 'block_paramtest'), ' ');
        $mform->setType('quizpasswordchk', PARAM_INT);

        $mform->addElement('advcheckbox', 'subnetchk', get_string('subnet', 'block_paramtest'), ' '); // voir la valeur par défaut sur le formulaire Moodle
        $mform->setType('subnetchk', PARAM_INT);
        $mform->addElement('textarea', 'subnet', '', 'rows="10" cols="80"');
        $mform->setType('subnet', PARAM_TEXT);
        $mform->hideIf('subnet', 'subnetchk', 'notchecked');
        $mform->addHelpButton('subnet', 'subnet', 'block_paramtest');

        // Decimal points of the grade (by default 2)
        $mform->addElement('hidden', 'decimalpoints', '2');
        $mform->setType('decimalpoints', PARAM_INT);

        // Decimal points of the questions (by default that is the same as decimalpoints)
        $mform->addElement('hidden', 'questiondecimalpoints', '-1');
        $mform->setType('questiondecimalpoints', PARAM_INT);

        // Preferred Behaviour (by default 'deferredfeedback')
        $mform->addElement('hidden', 'preferredbehaviour', 'deferredfeedback'); // voir la valeur par défaut sur le formulaire Moodle
        $mform->setType('preferredbehaviour', PARAM_TEXT);
        $mform->addElement('hidden', 'gradepass', 0); // voir la valeur par défaut sur le formulaire moodle
        $mform->setType('gradepass', PARAM_FLOAT);
        $mform->addElement('hidden', 'grademethod', '1'); // voir la valeur par défaut sur le formulaire moodle
        $mform->setType('grademethod', PARAM_INT);
        $mform->addElement('hidden', 'presetid', $this->presetid);
        $mform->setType('presetid', PARAM_INT);

        $submitlabal = null; // Default
        if ($this->isadding) {
            $submitlabal = get_string('addnewpreset', 'block_paramtest');
        }

        $this->add_action_buttons(true, $submitlabal);
    }

    /**
     * Set data to the form if necessary
     */
    function set_data($default_values) {
        parent::set_data($default_values);
    }

    /**
     * Validation method
     *
     * @param array $data
     * @param array $files
     * @return array
     */
    function validation($data, $files) {
        $errors = parent::validation($data, $files);

        return $errors;
    }

    /**
     * get data of the form
     *
     * @return object|null
     */
    function get_data() {
        $data = parent::get_data();

        return $data;
    }
}