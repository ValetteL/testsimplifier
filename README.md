Block Paramtest

This moodle block assist quiz creation in a Moodle course

It can list, add, or modify quizzes associated to a course when the block is added to the course
